## System Update Skill
This skill allows you to update your GNU/Linux.

## Description
This skill needs xterm to execute the update. The following OS releases are supported:
Arch Linux
Debian
CentOS

## Examples
* "Update my system"

## Credits
xenrox
